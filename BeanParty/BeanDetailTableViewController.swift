//
//  BeanDetailTableViewController.swift
//  BeanParty
//
//  Created by Paweł Jurczyk on 05/12/2015.
//  Copyright © 2015 Paweł Jurczyk. All rights reserved.
//

import UIKit

class BeanDetailTableViewController: UITableViewController, PTDBeanDelegate {
	var bean:PTDBean!
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.bean.delegate = self
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		self.bean.sendSerialString("Hello world")
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = UITableViewCell(style: .Default, reuseIdentifier: nil)
		cell.textLabel?.text = "Read temperature"
		return cell
	}
	
	func bean(bean: PTDBean!, didUpdateAccelerationAxes acceleration: PTDAcceleration) {
		let message = "x:\(acceleration.x) y:\(acceleration.y) z:\(acceleration.z)"
		let alert = UIAlertController(title: "AccelerationAxes", message: message, preferredStyle: .Alert)
		alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
		self.presentViewController(alert, animated: true, completion: nil)
	}
	
	func bean(bean: PTDBean!, didUpdateLedColor color: UIColor!) {
		var red:CGFloat = 0, green:CGFloat = 0, blue:CGFloat = 0, alpha:CGFloat = 0
		color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
		let message = "red:\(red) green:\(green) blue:\(blue)"
		let alert = UIAlertController(title: "Color read", message: message, preferredStyle: .Alert)
		alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
		self.presentViewController(alert, animated: true, completion: nil)
	}
	
	func bean(bean: PTDBean!, didUpdateTemperature degrees_celsius: NSNumber!) {
		let alert = UIAlertController(title: "Temperature", message: "\(degrees_celsius)C", preferredStyle: .Alert)
		alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
		self.presentViewController(alert, animated: true, completion: nil)
	}
	
	func bean(bean: PTDBean!, error: NSError!) {
		let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .Alert)
		alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
		self.presentViewController(alert, animated: true, completion: nil)
	}
	
	var collctedString:NSMutableString = NSMutableString()
	var timer:NSTimer?
	func bean(bean: PTDBean!, serialDataReceived data: NSData!) {
		self.timer?.invalidate()
		let string = NSString(data: data, encoding: NSUTF8StringEncoding) as! String
		self.collctedString.appendString(string)
		
		if self.collctedString.length > 0 {
			self.timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "presentAlert", userInfo: nil, repeats: false)
		}
	}
	
	func presentAlert() {
		let alert = UIAlertController(title: "Received", message: self.collctedString as String, preferredStyle: .Alert)
		alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
		self.presentViewController(alert, animated: true, completion: nil)
		self.collctedString = NSMutableString()
	}
	
	func beanDidUpdateArduinoPowerState(bean: PTDBean!) {
		
	}

}