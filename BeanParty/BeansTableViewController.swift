//
//  BeansTableViewController.swift
//  BeanParty
//
//  Created by Paweł Jurczyk on 05/12/2015.
//  Copyright © 2015 Paweł Jurczyk. All rights reserved.
//

import UIKit

class BeansTableViewController: UITableViewController, PTDBeanManagerDelegate, PTDBeanDelegate {
	let beanManager = PTDBeanManager()
	var beansDictionary = [String:PTDBean]()
	var connectedBean:PTDBean?
	@IBOutlet var disconnectBarButton: UIBarButtonItem!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.navigationItem.rightBarButtonItem = nil
		self.beanManager.delegate = self
	}
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		self.beanManager.delegate = self
	}
	
	@IBAction func disconnectFromBean(sender: UIBarButtonItem) {
		self.beanManager.disconnectBean(self.connectedBean, error: nil)
		self.tableView.reloadData()
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.beansDictionary.count
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let cell = tableView.cellForRowAtIndexPath(indexPath) as! BeanTableViewCell
		cell.bean.delegate = self
		self.beanManager.connectToBean(cell.bean, error: nil)
		self.beanManager.delegate = self
		tableView.reloadData()
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("BeanCell", forIndexPath: indexPath) as! BeanTableViewCell
		let beans = self.beansDictionary.map { $0.1 }
		cell.bean = beans[indexPath.row]
		return cell
	}
	
//MARK - BeanManagerDelegate Callbacks
	
	func beanManagerDidUpdateState(beanManager: PTDBeanManager!) {
		if beanManager.state == .PoweredOn {
			beanManager.startScanningForBeans_error(nil)
		} else if beanManager.state == .PoweredOff {
			let alert = UIAlertController(title: "Error", message: "Turn on bluetooth to continue", preferredStyle: .Alert)
			alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
			self.presentViewController(alert, animated: true, completion: nil)
		}
	}
	
	func beanManager(beanManager: PTDBeanManager!, didDiscoverBean bean: PTDBean!, error: NSError!) {
		let identifier = "\(bean.identifier)"
		if self.beansDictionary[identifier] == nil {
			self.beansDictionary[identifier] = bean
			self.tableView.reloadData()
		}
	}
	
	func beanManager(beanManager: PTDBeanManager!, didConnectBean bean: PTDBean!, error: NSError!) {
		self.tableView.reloadData()
		self.navigationItem.rightBarButtonItem = self.disconnectBarButton
		self.connectedBean = bean
		self.performSegueWithIdentifier("BeanDetailsSegue", sender: self)
	}
	
	func beanManager(beanManager: PTDBeanManager!, didDisconnectBean bean: PTDBean!, error: NSError!) {
		self.tableView.reloadData()
		self.navigationItem.rightBarButtonItem = nil
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "BeanDetailsSegue" {
			let beanDetail = segue.destinationViewController as! BeanDetailTableViewController
			beanDetail.bean = self.connectedBean
		}
	}
}

class BeanTableViewCell: UITableViewCell {
	
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var rssiLabel: UILabel!
	@IBOutlet weak var statusLabel: UILabel!
	var bean:PTDBean!
	
	override func layoutSubviews() {
		self.nameLabel.text = "Name: \(self.bean.name)"
		self.rssiLabel.text = "RSSI: \(self.bean.RSSI)"
		
		let statusText:String
		switch (self.bean.state) {
		case .Unknown:
			statusText = "Unknown"
		case .Discovered:
			statusText = "Disconnected"
		case .AttemptingConnection:
			statusText = "Connecting..."
		case .AttemptingValidation:
			statusText = "Validating..."
		case .ConnectedAndValidated:
			statusText = "Connected"
		case .AttemptingDisconnection:
			statusText = "Disconnecting..."
		}
		
		self.statusLabel.text = "State: \(statusText)"
	}
}
